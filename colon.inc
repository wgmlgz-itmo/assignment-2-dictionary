%define dict_ptr 0
%macro colon 2
  %ifstr %1
    %ifid %2
      %2: 
        dq dict_ptr       
        db %1, 0
        %define dict_ptr %2
    %else
      %error "label does not exist"
    %endif
  %else
    %error "key is not string"
  %endif
%endmacro
