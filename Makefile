ASM_BIN=nasm
ASMFLAGS=-f elf64 -g
LD_BIN=ld
PY=python3

ASM_FILES=lib.asm dict.asm main.asm
OBJ_FILES=$(ASM_FILES:.asm=.o)

OUT=main
.PHONY: clean test


%.o: %.asm
	$(ASM_BIN) $(ASMFLAGS) -o $@ $<

$(OUT): $(OBJ_FILES) 
	$(LD_BIN) -o $@ $^

clean:
	rm -rf *.o $(OUT)

test:  $(OUT)
	$(PY) test.py

