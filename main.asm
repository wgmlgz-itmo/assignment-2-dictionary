%include "lib.inc"
%include "dict.inc"


section .rodata

%include "words.inc"
undefined_message: db "undefined", 0

section .text
global _start

%define offset 8
%define buff_len 255

_start:
    push 0
    mov rdi, rsp
    mov rsi, buff_len
    call read_word
    mov rdi, rsp
    mov rsi, dict
    call find_word
    test rax, rax
    je .not_found
    mov rdi, rax
    add rdi, offset
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    jmp .end
  .not_found:
    mov rdi, undefined_message
    call print_error
  .end:
    add rsp, 8
    mov rax, 0
    call exit
