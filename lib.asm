section .data
%define stdout 1
%define stderr 2
section .text


global exit
global string_length
global string_equals
global string_copy
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int



; Принимает код возврата и завершает текущий процесс
exit: 
    mov rdi, rax
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax            ; Clear RAX (to store the length)
    test rdi, rdi
    je .done
    .loop:
        cmp byte [rdi], 0      ; Compare the current character with null terminator
        je .done                ; If it's the null terminator, we're done
        inc rdi                 ; Move to the next character
        inc rax                 ; Increment the length
        jmp .loop               ; Repeat the loop
    .done:
        ret                     ; Return with the length in RAX

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    test rdi, rdi
    je .end
    push rdi
    call string_length
    mov rdx, rax ; size
    mov rdi, stdout ;discriptor
    mov rax, 1; call
    pop rsi ; ptr
    syscall

    .end: ret

print_error:
    test rdi, rdi
    je .end
    push rdi
    call string_length
    mov rdx, rax ; size
    mov rdi, stderr ;discriptor
    mov rax, 1; call
    pop rsi ; ptr
    syscall

    .end: ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1 ; size
    mov rdi, stdout ;discriptor
    mov rax, 1; call
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rdx
    push r9
    push rax
    mov rax, rdi
    mov r9, rsp ; r9 = current pos to write
    sub rsp, 32
    mov rcx, 10 ; setup base
    dec r9
    mov [r9], byte 0
    test rax, rax
    jne .nonzero
    dec r9
    mov [r9], byte '0'
    jmp .print
    .nonzero:
        test rax, rax
        je .print
        xor rdx, rdx
        div rcx
        add rdx, '0'
        dec r9
        mov [r9], dl
        jmp .nonzero
    .print:
        mov rdi, r9
        call print_string
        add rsp, 32
        pop rax
        pop r9
        pop rdx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .more
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .more:
      call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
    .loop:
        mov al, [rdi]
        mov bl, [rsi]
        cmp al, bl  ; if chars not equal return
        jne .ne  
        test al, al  ; if a=0, then b=0, so equal
        je .eq
        inc rdi
        inc rsi
        jmp .loop
    .eq:
        pop rbx
        mov rax, 1
        ret
    .ne:
        pop rbx
        xor rax, rax
        ret 


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdx
    push rsi
    push rdi
    xor rax, rax 
    xor rdi, rdi
    push 0  
    mov rsi, rsp   
    mov rdx, 1 
    syscall
    pop rax
    pop rdi
    pop rsi
    pop rdx
    test rax, rax
    je .end_of_input
    ret
    .end_of_input:
        xor rax, rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r10 ; fin ptr
    push r9 ; fin ptr
    push r15 ; fin ptr
    xor rdx, rdx
    mov r10, rdi  ; r8 - cur
    push rdi
    xor r9, r9
    add rsi, rdi ; rsi - fin
    xor r15, r15
    add r15, rsi
    .loop:
        push r9
        push r10
        push rsi
        call read_char
        pop rsi
        pop r10
        pop r9
        test r9, r9
        jne .nofirst
        cmp rax, 0x20
        je .loop
        cmp rax, 0x9
        je .loop
        cmp rax, 0xA
        je .loop
        inc r9
    .nofirst:
        cmp rax, 0x20
        je .eend
        cmp rax, 0x9
        je .eend
        cmp rax, 0xA
        je .eend
        mov [r10], rax
        inc r10
        inc rdx
        cmp rsi, r10
        je .fail
        xor r15, r15
        mov [r10], r15
        test rax, rax
        je .end_zero
        jmp .loop
    .fail:
        pop rax
        pop r15
        pop r9
        pop r10
        xor rax, rax
        xor rdx, rdx
        ret
    .end_zero:
        dec rdx
    .eend:
        pop rax
        pop r15
        pop r9
        pop r10
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r10 ; tmp
    push r11 ; res
    push rbx
    push rdi

    xor rax, rax
    xor rdx, rdx
    .loop:
        xor rbx, rbx
        mov bl, byte [rdi]
        cmp rbx, '0'
        jl .end
        cmp rbx, '9'
        jg .end
        inc rdx
        sub rbx, '0'
        push rdx
        mov r10, 10
        mul r10
        add rax, rbx
        pop rdx
        inc rdi
        jmp .loop
    .end:
        pop rdi
        pop rbx
        pop r11
        pop r10
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], '-'
    jne .pos
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
.pos:
    jmp parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ; str
    push rsi ; buff
    call string_length
    pop rsi
    pop rdi
    push rax ; str_len
    inc rax
    cmp rax, rdx
    jle .loop_copy
    pop rax
    xor rax, rax
    ret
    .loop_copy:
        mov al, [rdi]
        mov [rsi], al
        test al, al
        je .fin
        inc rdi
        inc rsi
        jmp .loop_copy
    .fin:
    pop rax
    ret