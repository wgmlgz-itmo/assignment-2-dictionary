#!/usr/bin/python3

__unittest = True

import subprocess
import re
import unittest

# import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE

error_msg = "Didn't found a key"


class DictionaryTest(unittest.TestCase):
    # from lab1/test.py
    def launch(self, input):
        output = b""
        try:
            p = Popen(["./main"], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, errors) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, "segmentation fault")
            return (output.decode(), errors.decode())
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, "segmentation fault")
            return (exc.output.decode(), exc.returncode)

    def test_keyword(self):
        inputs = ["arasaka", "dog", "amogus"]
        outputs = ["tower", "town", "imposter"]
        for input, output in zip(inputs, outputs):
            self.assertEqual(self.launch(input), (output, ""))

    def test_not_found(self):
        inputs = ["afsfsdfdsfaf", "nofuture", "susser"]
        for input in inputs:
            self.assertEqual(self.launch(input), ("", "undefined"))

    def test_too_long(self):
        out = ("", "undefined")
        self.assertEqual(self.launch("A" * 270), out)

if __name__ == "__main__":
    unittest.main()
